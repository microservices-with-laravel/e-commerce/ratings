<?php

namespace App\Repositories;

use App\Models\Product;
use App\Models\ProductRating;
use Illuminate\Support\Collection;

class ProductRepository
{
    public function create(int $id, string $name): Product
    {
        return Product::create([
            'id' => $id,
            'name' => $name,
        ]);
    }

    public function rate(Product $product, float $rating, ?string $comment): float
    {
        ProductRating::create([
            'product_id' => $product->id,
            'rating' => $rating,
            'comment' => $comment,
        ]);

        return $this->updateAverageRating($product);
    }

    /**
     * @param Collection<int> $ids
     * @return Collection<Product>
     */
    public function getByIdsWithRatings(Collection $ids): Collection
    {
        return Product::with('ratings')
            ->whereIn('id', $ids)
            ->get();
    }

    private function updateAverageRating(Product $product): float
    {
        $product->average_rating = $product->ratings->avg('rating');
        $product->save();

        return $product->average_rating;
    }
}
