<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetRatingsRequest;
use App\Http\Requests\RateProductRequest;
use App\Http\Resources\ProductResource;
use App\Http\Resources\RatingResource;
use App\Models\Product;
use App\Services\ProductService;
use Illuminate\Http\Response;

class RatingController extends Controller
{
    public function __construct(private ProductService $productService)
    {
    }

    public function store(Product $product, RateProductRequest $request)
    {
        $productRatingContainer = $this->productService->rate(
            $product,
            $request->getRating(),
            $request->getComment()
        );

        return response([
            'data' => $productRatingContainer
        ], Response::HTTP_CREATED);
    }

    public function index(GetRatingsRequest $request)
    {
        $products = $this->productService
            ->getByIdsWithRatings(
                collect($request->getProductIds())
            );

        return RatingResource::collection($products);
    }

    public function get(Product $product)
    {
        return [
            'data' => new ProductResource($product)
        ];
    }
}
