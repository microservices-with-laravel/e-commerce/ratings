<?php

namespace App\Services;

use App\Models\Product;
use App\Models\ProductRating;
use App\Repositories\ProductRepository;
use Ecommerce\Common\Containers\Product\ProductContainer;
use Ecommerce\Common\Containers\Rating\ProductRatingContainer;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ProductService
{
    public function __construct(private ProductRepository $products, private RedisService $redis)
    {
    }

    public function create(ProductContainer $productContainer): Product
    {
        return $this->products->create($productContainer->id, $productContainer->name);
    }

    public function rate(
        Product $product,
        float $rating,
        ?string $comment
    ): ProductRatingContainer {
        return DB::transaction(function () use ($product, $rating, $comment) {
            $averageRating = $this->products->rate(
                $product,
                $rating,
                $comment
            );

            $container = $this->createContainer(
                $product,
                $rating,
                $averageRating
            );

            $this->redis->publishProductRated($container);
            return $container;
        });
    }

    /**
     * @param Collection<int> $ids
     * @return Collection<float>
     */
    public function getByIdsWithRatings(Collection $ids): Collection
    {
        return $this->products->getByIdsWithRatings($ids);
    }

    private function createContainer(Product $product, float $rating, float $averageRating): ProductRatingContainer
    {
        return new ProductRatingContainer(
            $product->id,
            $rating,
            round($averageRating, 2)
        );
    }
}
