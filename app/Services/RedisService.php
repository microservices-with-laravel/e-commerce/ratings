<?php

namespace App\Services;

use Ecommerce\Common\Containers\Rating\ProductRatingContainer;
use Ecommerce\Common\Events\Rating\ProductRatedEvent;
use Ecommerce\Common\Services\RedisService as BaseRedisService;

class RedisService extends BaseRedisService
{
    public function getServiceName(): string
    {
        return 'ratings';
    }

    public function publishProductRated(ProductRatingContainer $productRatingContainer): void
    {
        $this->publish(new ProductRatedEvent($productRatingContainer));
    }
}

